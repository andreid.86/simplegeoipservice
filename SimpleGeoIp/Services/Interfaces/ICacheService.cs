﻿using Core.BaseModels;

namespace Services.Interfaces
{
    public interface ICacheService
    {
        ContinentBase GetContinent(int continentId);
        CountryBase GetCountry(int countryId);
        GeoNameBase GetGeoName(long geoNameId);
        SubDivisionBase GetFirstSubDivision(int subDivisionId);
        SubDivisionBase GetSecondSubDivision(int subDivisionId);
        TimeZoneBase GetTimeZone(int timeZoneId);
    }
}
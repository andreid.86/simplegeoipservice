﻿using Services.Dtos;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IGeoIpService
    {
        Task<GeoIpInfo> GetGeoIpInfo(string ipAddress);
    }
}
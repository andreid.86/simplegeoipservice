﻿using Core.BaseModels;
using Core.Utils.Extensions;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Services.Interfaces;
using System.Collections.Concurrent;
using System.Data;
using System.Linq;
using System.Threading;

namespace Services.Implementations
{
    public sealed class CacheService : ICacheService
    {
        private static readonly Mutex CacheLock = new Mutex(false, "Cache");

        private ConcurrentDictionary<long, GeoNameBase> _cachedGeoNames;
        private ConcurrentDictionary<int, ContinentBase> _cachedContinents;
        private ConcurrentDictionary<int, SubDivisionBase> _cachedFirstSubDivisions;
        private ConcurrentDictionary<int, SubDivisionBase> _cachedSecondSubDivisions;
        private ConcurrentDictionary<int, TimeZoneBase> _cachedTimeZones;
        private ConcurrentDictionary<int, CountryBase> _cachedCountries;

        private readonly string _connectionString;

        public CacheService(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString(Core.Constants.SqlParameters.DefaultConnectionName);

            try
            {
                CacheLock.WaitOne();
                InitializeCacheStore();
            }
            finally
            {
                CacheLock.ReleaseMutex();
            }
        }

        private void InitializeCacheStore()
        {
            using (var conn = new SqlConnection(_connectionString))
            {

                _cachedGeoNames = conn.Query<GeoNameBase>(
                    $"SELECT * FROM {Core.Constants.TableNames.GeoNamesTableName}",
                    commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).ToConcurrentDictionary(key => key.Id, value => new GeoNameBase()
                    {
                        ContinentId = value.ContinentId,
                        CountryId = value.CountryId,
                        CityName = value.CityName,
                        FirstSubDivisionId = value.FirstSubDivisionId,
                        IsInEuropeanUnion = value.IsInEuropeanUnion,
                        SecondSubDivisionId = value.SecondSubDivisionId,
                        TimeZoneId = value.TimeZoneId
                    });

                _cachedContinents = conn.Query<ContinentBase>(
                    $"SELECT * FROM {Core.Constants.TableNames.ContinentsTableName}",
                    commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).ToConcurrentDictionary(key => key.Id, value => new ContinentBase()
                    {
                        Code = value.Code,
                        Name = value.Name
                    });


                _cachedCountries = conn.Query<CountryBase>(
                    $"SELECT * FROM {Core.Constants.TableNames.CountriesTableName}",
                    commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).ToConcurrentDictionary(key => key.Id, value => new CountryBase()
                    {
                        A2Code = value.A2Code,
                        Name = value.Name
                    });

                _cachedFirstSubDivisions = conn.Query<SubDivisionBase>(
                    $"SELECT * FROM {Core.Constants.TableNames.FirstSubDivisionsTableName}",
                    commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).ToConcurrentDictionary(key => key.Id, value => new SubDivisionBase()
                    {
                        Name = value.Name,
                        IsoCode = value.IsoCode
                    });

                _cachedSecondSubDivisions = conn.Query<SubDivisionBase>(
                    $"SELECT * FROM {Core.Constants.TableNames.SecondSubDivisionsTableName}",
                    commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).ToConcurrentDictionary(key => key.Id, value => new SubDivisionBase()
                    {
                        Name = value.Name,
                        IsoCode = value.IsoCode
                    });

                _cachedTimeZones = conn.Query<TimeZoneBase>(
                    $"SELECT * FROM {Core.Constants.TableNames.TimeZonesTableName}",
                    commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).ToConcurrentDictionary(key => key.Id, value => new TimeZoneBase()
                    {
                        Name = value.Name,
                        UtcOffset = value.UtcOffset,
                        Dst = value.Dst
                    });
            }
        }

        public ContinentBase GetContinent(int continentId)
        {
            try
            {
                CacheLock.WaitOne();

                if (_cachedContinents.ContainsKey(continentId))
                    return _cachedContinents[continentId];

                

                else
                {
                    CacheLock.ReleaseMutex();

                    using (var conn = new SqlConnection(_connectionString))
                    {
                        var parameters = new DynamicParameters();
                        parameters.Add("@id", continentId, DbType.Int32);

                        var dbItem = conn.Query<ContinentBase>(
                            $"SELECT * FROM {Core.Constants.TableNames.CountriesTableName} WHERE Id = @id",
                            parameters,
                            commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).Single();

                        CacheLock.WaitOne();
                        _cachedContinents[dbItem.Id] = dbItem;


                        return dbItem;
                    }

                }
            }
            finally
            {
                CacheLock.ReleaseMutex();
            }

        }

        public CountryBase GetCountry(int countryId)
        {
            try
            {
                CacheLock.WaitOne();

                if (_cachedCountries.ContainsKey(countryId))
                    return _cachedCountries[countryId];

                else
                {
                    CacheLock.ReleaseMutex();
                    using (var conn = new SqlConnection(_connectionString))
                    {
                        var parameters = new DynamicParameters();
                        parameters.Add("@id", countryId, DbType.Int32);

                        var dbItem = conn.Query<CountryBase>(
                            $"SELECT * FROM {Core.Constants.TableNames.CountriesTableName} WHERE Id = @id",
                            parameters,
                            commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).Single();

                        CacheLock.WaitOne();
                        _cachedCountries[dbItem.Id] = dbItem;

                        return dbItem;
                    }

                }
            }
            finally
            {
                CacheLock.ReleaseMutex();
            }

        }

        public TimeZoneBase GetTimeZone(int timeZoneId)
        {
            try
            {
                CacheLock.WaitOne();

                if (_cachedTimeZones.ContainsKey(timeZoneId))
                    return _cachedTimeZones[timeZoneId];

                else
                {
                    CacheLock.ReleaseMutex();
                    using (var conn = new SqlConnection(_connectionString))
                    {
                        var parameters = new DynamicParameters();
                        parameters.Add("@id", timeZoneId, DbType.Int32);

                        var dbItem = conn.Query<TimeZoneBase>(
                            $"SELECT * FROM {Core.Constants.TableNames.TimeZonesTableName} WHERE Id = @id",
                            parameters,
                            commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).Single();

                        CacheLock.WaitOne();
                        _cachedTimeZones[dbItem.Id] = dbItem;

                        return dbItem;
                    }

                }
            }
            finally
            {
                CacheLock.ReleaseMutex();
            }
        }

        public GeoNameBase GetGeoName(long geoNameId)
        {
            try
            {
                CacheLock.WaitOne();

                if (_cachedGeoNames.ContainsKey(geoNameId))
                    return _cachedGeoNames[geoNameId];

                else
                {
                    CacheLock.ReleaseMutex();
                    using (var conn = new SqlConnection(_connectionString))
                    {
                        var parameters = new DynamicParameters();
                        parameters.Add("@id", geoNameId, DbType.Int64);

                        var dbItem = conn.Query<GeoNameBase>(
                            $"SELECT * FROM {Core.Constants.TableNames.GeoNamesTableName} WHERE Id = @id",
                            parameters,
                            commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).Single();

                        CacheLock.WaitOne();
                        _cachedGeoNames[dbItem.Id] = dbItem;

                        return dbItem;
                    }

                }
            }
            finally
            {
                CacheLock.ReleaseMutex();
            }
        }

        public SubDivisionBase GetFirstSubDivision(int subDivisionId)
        {
            return GetSubDivision(subDivisionId, "first");
        }

        public SubDivisionBase GetSecondSubDivision(int subDivisionId)
        {
            return GetSubDivision(subDivisionId, "second");
        }

        private SubDivisionBase GetSubDivision(int subDivisionId, string type)
        {
            ConcurrentDictionary<int, SubDivisionBase> cacheStore;
            string dbTableName;

            switch (type.ToUpper())
            {
                case "FIRST":
                    cacheStore = _cachedFirstSubDivisions;
                    dbTableName = Core.Constants.TableNames.FirstSubDivisionsTableName;
                    break;
                case "SECOND":
                    cacheStore = _cachedSecondSubDivisions;
                    dbTableName = Core.Constants.TableNames.SecondSubDivisionsTableName;
                    break;
                default:
                    return null;
            }

            try
            {
                CacheLock.WaitOne();

                if (cacheStore.ContainsKey(subDivisionId))
                    return cacheStore[subDivisionId];

                else
                {
                    CacheLock.ReleaseMutex();
                    using (var conn = new SqlConnection(_connectionString))
                    {
                        var parameters = new DynamicParameters();
                        parameters.Add("@id", subDivisionId, DbType.Int32);

                        var dbItem = conn.Query<SubDivisionBase>(
                            $"SELECT * FROM {dbTableName} WHERE Id = @id",
                            parameters,
                            commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds).Single();

                        CacheLock.WaitOne();
                        cacheStore[dbItem.Id] = dbItem;

                        return dbItem;
                    }

                }
            }
            finally
            {
                CacheLock.ReleaseMutex();
            }

        }
    }
}
﻿using System;
using System.Net;
using System.Threading.Tasks;
using Core.Utils;
using Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Services.Dtos;
using Services.Interfaces;

namespace Services.Implementations
{
    public sealed class GeoIpService : IGeoIpService
    {
        private readonly DataContext _context;
        private readonly ICacheService _cache;

        public GeoIpService(ICacheService cache, DataContext context)
        {
            _context = context;
            _cache = cache;
        }

        public async Task<GeoIpInfo> GetGeoIpInfo(string ipAddress)
        {

            int ip = IpHelpers.IpToInt(ipAddress);

            var network = await _context.Networks.FirstOrDefaultAsync(n =>
                (n.MinIpAddress <= ip) &&
                (ip <= n.MaxIpAddress) &&
                (Math.Abs(n.MinIpAddress) != Math.Abs(n.MaxIpAddress)));

            if (network == null) return null;

            var geoInfo = new GeoIpInfo
            {
                Network = network.Name,
                AccuracyRadius = network.AccuracyRadius,
                StartIp = IpHelpers.IntToIp(network.MinIpAddress),
                EndIp = IpHelpers.IntToIp(network.MaxIpAddress),
                Longitude = (decimal)network.Location.Coordinate.X,
                Latitude = (decimal)network.Location.Coordinate.Y,
            };

            if (network.RegisteredCountryId.HasValue)
            {
                var registeredCountry = _cache.GetCountry(network.RegisteredCountryId.Value);
                geoInfo.RegisteredCountry = new CountryInfo(registeredCountry.Name, registeredCountry.A2Code);
            }

            if (network.RepresentedCountryId.HasValue)
            {
                var representedCountry = _cache.GetCountry(network.RepresentedCountryId.Value);
                geoInfo.RepresentedCountry = new CountryInfo(representedCountry.Name, representedCountry.A2Code);
            }

            if (network.GeoNameId.HasValue)
            {
                var geoName = _cache.GetGeoName(network.GeoNameId.Value);
                var continent = _cache.GetContinent(geoName.ContinentId);
                geoInfo.Continent = new ContinentInfo(continent.Name, continent.Code);

                if (geoName.FirstSubDivisionId.HasValue)
                {
                    var firstSubDivision = _cache.GetFirstSubDivision(geoName.FirstSubDivisionId.Value);
                    geoInfo.FirstSubDivisionName = firstSubDivision.Name;
                }

                if (geoName.SecondSubDivisionId.HasValue)
                {
                    var secondSubDivision = _cache.GetSecondSubDivision(geoName.SecondSubDivisionId.Value);
                    geoInfo.SecondSubDivisionName = secondSubDivision.Name;
                }

                geoInfo.City = geoName.CityName;
                geoInfo.IsInEuropeanUnion = geoName.IsInEuropeanUnion;
                geoInfo.TimeZoneName = _cache.GetTimeZone(geoName.TimeZoneId).Name;

            }

            return geoInfo;
        }
    }
}

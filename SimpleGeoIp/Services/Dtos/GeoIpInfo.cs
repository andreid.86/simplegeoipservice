﻿namespace Services.Dtos
{
    public class GeoIpInfo
    {
        public string Network { get; set; }
        public string StartIp { get; set; }
        public string EndIp { get; set; }
        public ContinentInfo Continent { get; set; }
        public CountryInfo RegisteredCountry { get; set; }
        public CountryInfo RepresentedCountry { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public int? AccuracyRadius { get; set; }
        public string FirstSubDivisionName { get; set; }
        public string SecondSubDivisionName { get; set; }
        public string City { get; set; }
        public bool IsInEuropeanUnion { get; set; }
        public string TimeZoneName { get; set; }
    }

    public class CountryInfo
    {
        public CountryInfo(string name, string a2Code)
        {
            Name = name;
            A2Code = a2Code;
        }

        public string Name { get; set; }
        public string A2Code { get; set; }
    }

    public class ContinentInfo
    {
        public ContinentInfo(string name, string code)
        {
            Name = name;
            Code = code;
        }
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class TimeZoneInfo
    {
        public string TimeZone { get; set; }
        public int? UtcOffset { get; set; }
        public bool? DaySavingTime { get; set; }
    }
}
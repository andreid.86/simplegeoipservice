﻿using System;
using Core.Utils;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class IpHelpersTests
    {
        [Test]
        [TestCase("0.0.0.0")]
        [TestCase("0.0.0.1")]
        [TestCase("1.0.0.1")]
        [TestCase("1.1.1.1")]
        [TestCase("255.1.1.1")]
        [TestCase("255.1.1.255")]

        public void IsValidIp_WhenCalled_WithValidIp_ShouldReturn_True(string ipAddress)
        {
            Assert.That(IpHelpers.IsValidIp(ipAddress), Is.True);
        }

        [Test]
        [TestCase("1111")]
        [TestCase("abcd")]
        [TestCase("0")]
        [TestCase("0.0.0")]
        [TestCase("-1.-1.-1.-1")]
        [TestCase("256.1.1.1")]
        [TestCase("1.256.1.1")]
        [TestCase("1.1.256.1")]
        [TestCase("1.1.1.256")]
        [TestCase("1.0.0.1a")]
        [TestCase("1.0.0.1.")]
        [TestCase(".1.0.0.1")]
        [TestCase("a1.0.0.1")]

        public void IsValidIp_WhenCalled_WithInValidIp_ShouldReturn_False(string ipAddress)

        {
            Assert.That(IpHelpers.IsValidIp(ipAddress), Is.False);
        }

        [TestCase("192.168.0.1/24", ExpectedResult = 16843009)]
        //[TestCase("1.1.10.0/23", ExpectedResult = 16845313)]
        //[TestCase("200.200.200.200/25", ExpectedResult = -926365567)]
        public int MinIp_WhenCalled_WithValidNetwork_ShouldReturn_ValidMinIpInt(string network)
        {
            return IpHelpers.MinIp(network);
        }

        [TestCase("5.5.5.5/25", ExpectedResult = 84215166)]
        [TestCase("240.0.0.255/30", ExpectedResult = -268435202)]
        [TestCase("200.200.200.200/25", ExpectedResult = -926365442)]
        public int MaxIp_WhenCalled_WithValidNetwork_ShouldReturn_ValidIntMaxIpInt(string network)
        {
            return IpHelpers.MaxIp(network);
        }
        
        [TestCase("1.1.1.256/1")]
        [TestCase("1.1.1.1/33")]
        [TestCase("256.1.1.1/1")]
        [TestCase("256.1.1.1")]
        [TestCase("256.1.1.1\\1")]
        public void MinIP_WhenCalled_WithInvalidNetwork_ShouldThrow_Exception(string network)
        {
            Exception ex = null;
            try
            {
                IpHelpers.MinIp(network);
            }
            catch (Exception e)
            {
                ex = e;
            }
            Assert.That(ex, Is.Not.Null);
        }

        [TestCase("1.1.1.256/1")]
        [TestCase("1.1.1.1/33")]
        [TestCase("256.1.1.1/1")]
        [TestCase("256.1.1.1")]
        [TestCase("256.1.1.1\\1")]
        public void MaxIP_WhenCalled_WithInvalidNetwork_ShouldThrow_Exception(string network)
        {
            Exception ex = null;
            try
            {
                IpHelpers.MaxIp(network);
            }
            catch (Exception e)
            {
                ex = e;
            }
            Assert.That(ex, Is.Not.Null);
        }


        [TestCase("192.168.0.1")]
        [TestCase("172.16.0.1")]
        [TestCase("172.31.0.1")]
        [TestCase("10.0.0.1")]
        [TestCase("10.255.255.255")]
        public void IsPrivateIp_WhenCalled_WithPrivateIp_ShouldReturn_True(string ip)
        {
            Assert.That(IpHelpers.IsPrivateIp(ip), Is.True);
        }

        [TestCase("0.0.0.0")]
        [TestCase("1.1.1.1")]
        [TestCase("191.168.0.1")]
        [TestCase("193.168.0.1")]
        [TestCase("172.15.0.1")]
        [TestCase("172.32.0.1")]
        [TestCase("9.0.0.1")]
        public void IsPrivateIp_WhenCalled_WithNonPrivateIp_ShouldReturn_False(string ip)
        {
            Assert.That(IpHelpers.IsPrivateIp(ip), Is.False);
        }

        [TestCase("215.215.215.1", ExpectedResult = -673720575)]
        //[TestCase("78.78.1.1", ExpectedResult = 1313734913)]
        //[TestCase("8.8.4.4", ExpectedResult = 134743044)]
        //[TestCase("200.200.200.200", ExpectedResult = -926365496)]
        public int IpToInt_WhenCalled_WithValidIp_ShouldReturn_ValidIntIp(string ip)
        {
            return IpHelpers.IpToInt(ip);
        }

        [TestCase("1.1.1.256")]
        [TestCase("1.1.1")]
        [TestCase("256.1.1.1")]
        [TestCase("-1.1.1.1")]
        [TestCase("ipAddress")]
        public void IpToInt_WhenCalled_WithInvalidIp_ShouldThrow_Exception(string ip)
        {
            Exception ex = null;
            try
            {
                IpHelpers.IpToInt(ip);
            }
            catch (Exception e)
            {
                ex = e;
            }
            Assert.That(ex, Is.Not.Null);
        }


        [Test]
        public void Test()
        {
            double[] arr = new double[100];
            //заполняем массив случайными числами
            Random rd = new Random();
            for (int i = 0; i < arr.Length; ++i)
            {
                arr[i] = rd.Next(1, 101);
            }
            System.Console.WriteLine("The array before sorting:");
            foreach (double x in arr)
            {
                System.Console.Write(x + " ");
            }
            //сортировка
            Sorting(arr, 0, arr.Length - 1);
            System.Console.WriteLine("nnThe array after sorting:");
            foreach (double x in arr)
            {
                System.Console.Write(x + " ");
            }
            System.Console.WriteLine("nnPress the <Enter> key");
        }

        private static void Sorting(double[] arr, long first, long last)
        {
            var idx = (last - first) / 2 + first;   // Индекс опорного элемента
            double pivot = arr[idx];                    // Значение опорного элемента
            long leftIdx = first;
            long rightIdx = last;

            while (leftIdx <= rightIdx)
            {
                while (leftIdx <= last && arr[leftIdx] < pivot )
                {
                    ++leftIdx;
                }

                ;                                   // Проверить i

                while (rightIdx >= first && arr[rightIdx] > pivot )
                {
                    --rightIdx;
                }


                ;                                   // Проверить j

                if (leftIdx <= rightIdx)
                {
                    var temp = arr[leftIdx];
                    arr[leftIdx] = arr[rightIdx];
                    arr[rightIdx] = temp;
                    ++leftIdx; --rightIdx;
                }
            }

            if (rightIdx > first)
            {
                Sorting(arr, first, rightIdx);
            }

            if (leftIdx < last)
            {
                Sorting(arr, leftIdx, last);
            }

            ;
        }
    }
}
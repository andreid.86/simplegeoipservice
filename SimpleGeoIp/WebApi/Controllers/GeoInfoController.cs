﻿using System;
using System.Threading.Tasks;
using Core.Utils;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeoInfoController : ControllerBase
    {
        private readonly IGeoIpService _geoIpService;

        public GeoInfoController(IGeoIpService geoIpService)
        {
            _geoIpService = geoIpService;
        }

        [HttpGet]
        public IActionResult Status()
        {
            return Ok("It works");
        }


        [HttpGet("{ipAddress}")]
        public async Task<IActionResult> GetGeoInfo(string ipAddress)
        {
            if (!IpHelpers.IsValidIp(ipAddress))
                return BadRequest("Invalid IP Address");

            if (IpHelpers.IsPrivateIp(ipAddress))
                return BadRequest("This is a private IP");

            var result = await _geoIpService.GetGeoIpInfo(ipAddress);

            if (result == null)
                return NoContent();
            
            return Ok(result);
        }
    }
}
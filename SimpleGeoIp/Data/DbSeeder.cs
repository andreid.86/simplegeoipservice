﻿namespace Data
{
    public sealed class DbSeeder
    {
        private readonly DataContext _dataContext;
        
        public DbSeeder(DataContext context)
        {
            _dataContext = context;
        }

        public void SeedDb()
        {

        }

    }
}
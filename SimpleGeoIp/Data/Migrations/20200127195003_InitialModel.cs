﻿using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace Data.Migrations
{
    public partial class InitialModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Continents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Continents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    A2Code = table.Column<string>(maxLength: 2, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FirstSubDivisions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    IsoCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FirstSubDivisions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SecondSubDivisions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    IsoCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecondSubDivisions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TimeZones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    UtcOffset = table.Column<int>(nullable: true),
                    Dst = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeZones", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeoNames",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false),
                    ContinentId = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    TimeZoneId = table.Column<int>(nullable: false),
                    FirstSubDivisionId = table.Column<int>(nullable: true),
                    SecondSubDivisionId = table.Column<int>(nullable: true),
                    CityName = table.Column<string>(maxLength: 255, nullable: true),
                    IsInEuropeanUnion = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeoNames", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GeoNames_Continents_ContinentId",
                        column: x => x.ContinentId,
                        principalTable: "Continents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GeoNames_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GeoNames_FirstSubDivisions_FirstSubDivisionId",
                        column: x => x.FirstSubDivisionId,
                        principalTable: "FirstSubDivisions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GeoNames_SecondSubDivisions_SecondSubDivisionId",
                        column: x => x.SecondSubDivisionId,
                        principalTable: "SecondSubDivisions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GeoNames_TimeZones_TimeZoneId",
                        column: x => x.TimeZoneId,
                        principalTable: "TimeZones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Networks",
                columns: table => new
                {
                    MinIpAddress = table.Column<int>(nullable: false),
                    MaxIpAddress = table.Column<int>(nullable: false),
                    GeoNameId = table.Column<long>(nullable: true),
                    RegisteredCountryId = table.Column<int>(nullable: true),
                    RepresentedCountryId = table.Column<int>(nullable: true),
                    Network = table.Column<string>(maxLength: 18, nullable: false),
                    Location = table.Column<Point>(nullable: true),
                    AccuracyRadius = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Networks", x => new { x.MinIpAddress, x.MaxIpAddress })
                        .Annotation("SqlServer:Clustered", true);
                    table.ForeignKey(
                        name: "FK_Networks_GeoNames_GeoNameId",
                        column: x => x.GeoNameId,
                        principalTable: "GeoNames",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Networks_Countries_RegisteredCountryId",
                        column: x => x.RegisteredCountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Networks_Countries_RepresentedCountryId",
                        column: x => x.RepresentedCountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Continents_Id",
                table: "Continents",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Countries_Id",
                table: "Countries",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FirstSubDivisions_Id",
                table: "FirstSubDivisions",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GeoNames_ContinentId",
                table: "GeoNames",
                column: "ContinentId");

            migrationBuilder.CreateIndex(
                name: "IX_GeoNames_CountryId",
                table: "GeoNames",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_GeoNames_FirstSubDivisionId",
                table: "GeoNames",
                column: "FirstSubDivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_GeoNames_Id",
                table: "GeoNames",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_GeoNames_SecondSubDivisionId",
                table: "GeoNames",
                column: "SecondSubDivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_GeoNames_TimeZoneId",
                table: "GeoNames",
                column: "TimeZoneId");

            migrationBuilder.CreateIndex(
                name: "IX_Networks_GeoNameId",
                table: "Networks",
                column: "GeoNameId");

            migrationBuilder.CreateIndex(
                name: "IX_Networks_RegisteredCountryId",
                table: "Networks",
                column: "RegisteredCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Networks_RepresentedCountryId",
                table: "Networks",
                column: "RepresentedCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_SecondSubDivisions_Id",
                table: "SecondSubDivisions",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TimeZones_Id",
                table: "TimeZones",
                column: "Id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Networks");

            migrationBuilder.DropTable(
                name: "GeoNames");

            migrationBuilder.DropTable(
                name: "Continents");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "FirstSubDivisions");

            migrationBuilder.DropTable(
                name: "SecondSubDivisions");

            migrationBuilder.DropTable(
                name: "TimeZones");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class UpdateNetworkStoredProcedureAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
			const string sqlCode =
					@"DROP PROCEDURE IF EXISTS [dbo].[UpdateNetworks]
                    DROP TYPE IF EXISTS [dbo].[NetworkTableType]
                    
                    CREATE TYPE [dbo].[NetworkTableType] AS TABLE
                    (
						GeoNameId bigint null,
						RegisteredCountryId int null,
	                    RepresentedCountryId int null,
						Name nvarchar(18) not null,
						MinIpAddress int not null,
	                    MaxIpAddress int not null,
	                    Location geography null,
	                    AccuracyRadius int null
                    )
                    
                    GO
                    
                    CREATE PROCEDURE [dbo].[UpdateNetworks]
	                    @networks [dbo].[NetworkTableType] READONLY
                        
	                    WITH RECOMPILE
	                    AS
                        
	                    BEGIN 
		                    MERGE INTO [dbo].[Networks] G 
		                    USING @networks TVP
		                    ON (TVP.MinIpAddress = G.MinIpAddress AND TVP.MaxIpAddress = G.MaxIpAddress)
		                    WHEN MATCHED THEN   
			                    UPDATE SET 
				                    Network = TVP.Name,
				                    MinIpAddress = TVP.MinIpAddress,
				                    MaxIpAddress = TVP.MaxIpAddress,
				                    GeoNameId = TVP.GeoNameId,
				                    RegisteredCountryId = TVP.RegisteredCountryId,
				                    RepresentedCountryId  = TVP.RepresentedCountryId,
				                    Location  = TVP.Location,
				                    AccuracyRadius = TVP.AccuracyRadius
		                    WHEN NOT MATCHED THEN  
			                    INSERT (Network, MinIpAddress, MaxIpAddress, GeoNameId, RegisteredCountryId, RepresentedCountryId, Location, AccuracyRadius) 
			                    VALUES (TVP.Name, TVP.MinIpAddress, TVP.MaxIpAddress, TVP.GeoNameId, TVP.RegisteredCountryId, TVP.RepresentedCountryId, TVP.Location, TVP.AccuracyRadius);  	                
	                    END
                    GO";

			migrationBuilder.Sql(sqlCode);
		}

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            const string sqlCode =
				    @"DROP PROCEDURE IF EXISTS [dbo].[UpdateNetworks]
                    DROP TYPE IF EXISTS [dbo].[NetworkTableType]";

            migrationBuilder.Sql(sqlCode);
		}
    }
}

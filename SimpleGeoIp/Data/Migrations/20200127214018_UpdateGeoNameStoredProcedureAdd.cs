﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class UpdateGeoNameStoredProcedureAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            const string sqlCode =
				@"DROP PROCEDURE IF EXISTS [dbo].[UpdateGeoNames]
                DROP TYPE IF EXISTS [dbo].[GeoNameTableType]

                CREATE TYPE [dbo].[GeoNameTableType] AS TABLE
                (
	                Id bigint not null,
	                ContinentId int not null,
	                CountryId int not null,
	                TimeZoneId int not null,
	                FirstSubDivisionId int null,
	                SecondSubDivisionId int null,
	                CityName nvarchar(255),
	                IsInEuropeanUnion bit not null
                )

                GO

                CREATE PROCEDURE [dbo].[UpdateGeoNames]
	                @geoNames [dbo].[GeoNameTableType] READONLY

                WITH RECOMPILE

                AS
                BEGIN 
	                SET TRANSACTION ISOLATION LEVEL SERIALIZABLE  
	                BEGIN TRANSACTION

		                MERGE INTO [dbo].[GeoNames] G 
		                USING @geoNames TVP
		                ON TVP.Id = G.Id
	                
		                WHEN MATCHED THEN   
			                UPDATE SET 
				                Id = TVP.Id,
				                ContinentId = TVP.ContinentId,
				                CountryId = TVP.CountryId,
				                TimeZoneId = TVP.TimeZoneId,
				                FirstSubDivisionId  = TVP.FirstSubDivisionId,
				                SecondSubDivisionId  = TVP.SecondSubDivisionId,
				                CityName = TVP.CityName,
				                IsInEuropeanUnion = TVP.IsInEuropeanUnion

		                WHEN NOT MATCHED THEN  
			                INSERT (Id, ContinentId, CountryId, TimeZoneId, FirstSubDivisionId, SecondSubDivisionId, CityName, IsInEuropeanUnion) 
			                VALUES (TVP.Id, TVP.ContinentId, TVP.CountryId, TVP.TimeZoneId, TVP.FirstSubDivisionId, TVP.SecondSubDivisionId, TVP.CityName, TVP.IsInEuropeanUnion);  
	                
		                IF @@ERROR <> 0
		                BEGIN
			                ROLLBACK
			                RETURN
		                END
	                
		                COMMIT
	                END
                GO";

            migrationBuilder.Sql(sqlCode);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            const string sqlCode =
                @"DROP PROCEDURE IF EXISTS [dbo].[UpdateGeoNames]
                DROP TYPE IF EXISTS [dbo].[GeoNameTableType]";

            migrationBuilder.Sql(sqlCode);
		}
    }
}

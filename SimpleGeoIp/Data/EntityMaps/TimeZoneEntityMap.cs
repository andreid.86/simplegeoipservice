﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.EntityMaps
{
    public class TimeZoneEntityMap : IEntityTypeConfiguration<TimeZoneEntity>
    {
        public void Configure(EntityTypeBuilder<TimeZoneEntity> builder)
        {
            builder.HasIndex(t => t.Id).IsUnique(true);
            builder.Property(c => c.Id).UseIdentityColumn().ValueGeneratedOnAdd();
            builder.Property(t => t.Name).IsRequired().HasMaxLength(255);

            builder.ToTable(Core.Constants.TableNames.TimeZonesTableName);
        }
    }
}
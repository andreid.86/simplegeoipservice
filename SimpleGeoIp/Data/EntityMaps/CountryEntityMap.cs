﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.EntityMaps
{
    public class CountryEntityMap : IEntityTypeConfiguration<CountryEntity>
    {
        public void Configure(EntityTypeBuilder<CountryEntity> builder)
        {
            builder.HasIndex(c => c.Id).IsUnique(true);
            builder.Property(c => c.Id).UseIdentityColumn().ValueGeneratedOnAdd();
            builder.Property(c => c.Name).IsRequired().HasMaxLength(255);
            builder.Property(c => c.A2Code).IsRequired().HasMaxLength(2);

            builder.ToTable(Core.Constants.TableNames.CountriesTableName);
        }
    }
}
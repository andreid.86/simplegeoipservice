﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.EntityMaps
{
    public class SecondSubDivisionEntityMap : IEntityTypeConfiguration<SecondSubDivisionEntity>
    {
        public void Configure(EntityTypeBuilder<SecondSubDivisionEntity> builder)
        {
            builder.HasIndex(s => s.Id).IsUnique(true);
            builder.Property(c => c.Id).UseIdentityColumn().ValueGeneratedOnAdd();
            builder.Property(s => s.Name).IsRequired().HasMaxLength(255);


            builder.ToTable(Core.Constants.TableNames.SecondSubDivisionsTableName);
        }
    }
}
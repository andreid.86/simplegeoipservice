﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.EntityMaps
{
    public class FirstSubDivisionEntityMap : IEntityTypeConfiguration<FirstSubDivisionEntity>
    {
        public void Configure(EntityTypeBuilder<FirstSubDivisionEntity> builder)
        {
            builder.HasIndex(f => f.Id).IsUnique(true);
            builder.Property(c => c.Id).UseIdentityColumn().ValueGeneratedOnAdd();
            builder.Property(f => f.Name).IsRequired().HasMaxLength(255);

            builder.ToTable(Core.Constants.TableNames.FirstSubDivisionsTableName);
        }
    }
}
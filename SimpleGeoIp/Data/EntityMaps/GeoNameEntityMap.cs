﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.EntityMaps
{
    public class GeoNameEntityMap : IEntityTypeConfiguration<GeoNameEntity>
    {
        public void Configure(EntityTypeBuilder<GeoNameEntity> builder)
        {
            builder.HasIndex(i => i.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();
            builder.Property(g => g.CityName).IsRequired(false).HasMaxLength(255);

            builder.HasOne(g => g.Continent)
                .WithMany(c => c.GeoNames)
                .HasForeignKey(g => g.ContinentId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(g => g.Country)
                .WithMany(c => c.GeoNames)
                .HasForeignKey(g => g.CountryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(g => g.TimeZone)
                .WithMany(t => t.GeoNames)
                .HasForeignKey(g => g.TimeZoneId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(g => g.FirstSubDivision)
                .WithMany(t => t.GeoNames)
                .HasForeignKey(g => g.FirstSubDivisionId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(g => g.SecondSubDivision)
                .WithMany(t => t.GeoNames)
                .HasForeignKey(g => g.SecondSubDivisionId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.ToTable(Core.Constants.TableNames.GeoNamesTableName);
        }
    }
}
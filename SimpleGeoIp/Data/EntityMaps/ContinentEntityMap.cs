﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.EntityMaps
{
    public class ContinentEntityMap : IEntityTypeConfiguration<ContinentEntity>
    {
        public void Configure(EntityTypeBuilder<ContinentEntity> builder)
        {
            builder.HasIndex(c => c.Id).IsUnique(true);
            builder.Property(c => c.Id).UseIdentityColumn().ValueGeneratedOnAdd();
            builder.Property(c => c.Name).IsRequired().HasMaxLength(255);
            builder.ToTable(Core.Constants.TableNames.ContinentsTableName);
        }
    }
}
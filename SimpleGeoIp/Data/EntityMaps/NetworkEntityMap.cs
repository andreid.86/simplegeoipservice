﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.EntityMaps
{
    public class NetworkEntityMap : IEntityTypeConfiguration<NetworkEntity>
    {
        public void Configure(EntityTypeBuilder<NetworkEntity> builder)
        {
            builder.Property(n => n.Name).IsRequired().HasMaxLength(18);
            builder.Property(n => n.Name).HasColumnName("Network");

            builder.HasKey(n => new { n.MinIpAddress, n.MaxIpAddress })
                .IsClustered(true);
            builder.HasOne(n => n.GeoName)
                .WithMany(g => g.Networks)
                .HasForeignKey(k => k.GeoNameId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(n => n.RegisteredCountry)
                .WithMany(g => g.NetworksRegistered)
                .HasForeignKey(k => k.RegisteredCountryId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(n => n.RepresentedCountry)
                .WithMany(g => g.NetworksRepresented)
                .HasForeignKey(k => k.RepresentedCountryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.ToTable(Core.Constants.TableNames.NetworksTableName);
        }
    }
}
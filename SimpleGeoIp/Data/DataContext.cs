﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<ContinentEntity> Continents { get; set; }
        public DbSet<FirstSubDivisionEntity> FirstSubDivisions { get; set; }
        public DbSet<GeoNameEntity> GeoNames { get; set; }
        public DbSet<NetworkEntity> Networks { get; set; }
        public DbSet<SecondSubDivisionEntity> SecondSubDivisions { get; set; }
        public DbSet<TimeZoneEntity> TimeZones { get; set; }
        public DbSet<CountryEntity> Countries { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
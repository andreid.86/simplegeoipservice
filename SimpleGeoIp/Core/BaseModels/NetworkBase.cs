﻿using NetTopologySuite.Geometries;

namespace Core.BaseModels
{
    public class NetworkBase
    {
        //A unique identifier for the network's location as specified by GeoNames. 
        public long? GeoNameId { get; set; }

        //The registered country is the country in which the ISP has registered the network.
        //This column contains a unique identifier for the network's registered country as specified by GeoNames. 
        public int? RegisteredCountryId { get; set; }

        //The represented country is the country which is represented by users of the IP address.
        //For instance, the country represented by an overseas military base.
        //This column contains a unique identifier for the network's registered country as specified by GeoNames.
        public int? RepresentedCountryId { get; set; }

        //This is the IPv4 network in CIDR format.
        public string Name { get; set; }
        
        private int _minIpAddress;
        public int MinIpAddress { get => _minIpAddress; set => _minIpAddress = unchecked(value); }

        private int _maxIpAddress;
        public int MaxIpAddress { get => _maxIpAddress; set => _maxIpAddress = unchecked(value) ; }

        public Point Location { get; set; }
        public int? AccuracyRadius { get; set; }


    }
}
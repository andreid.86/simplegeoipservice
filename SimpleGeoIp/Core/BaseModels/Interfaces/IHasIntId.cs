﻿namespace Core.BaseModels.Interfaces
{
    public interface IHasIntId
    {
        int Id { get; set; }
    }
}
﻿namespace Core.BaseModels.Interfaces
{
    public interface IHasLongId
    {
        long Id { get; set; }
    }
}
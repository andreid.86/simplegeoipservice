﻿namespace Core.BaseModels.Interfaces
{
    public interface IHasByteId
    {
        byte Id { get; set; }
    }
}
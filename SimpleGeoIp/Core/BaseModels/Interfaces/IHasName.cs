﻿namespace Core.BaseModels.Interfaces
{
    public interface IHasName
    {
        string Name { get; set; }
    }
}
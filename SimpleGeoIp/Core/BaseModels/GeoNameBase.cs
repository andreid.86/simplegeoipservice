﻿using Core.BaseModels.Interfaces;

namespace Core.BaseModels
{
    public class GeoNameBase : IHasLongId
    {
        public long Id { get; set; }
        public int ContinentId { get; set; }
        public int CountryId { get; set; }
        public int TimeZoneId { get; set; }

        public int? FirstSubDivisionId { get; set; }
        public int? SecondSubDivisionId { get; set; }

        public string CityName { get; set; }

        // This is true if the country associated with the location is a member state of the European Union.
        // It is false otherwise.
        public bool IsInEuropeanUnion { get; set; }
    }
}
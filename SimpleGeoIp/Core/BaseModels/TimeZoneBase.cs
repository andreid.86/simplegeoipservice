﻿using Core.BaseModels.Interfaces;

namespace Core.BaseModels
{
    public class TimeZoneBase : IHasName, IHasIntId
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? UtcOffset { get; set; }
        public bool? Dst { get; set; }
    }
}
﻿using Core.BaseModels.Interfaces;

namespace Core.BaseModels
{
    public class ContinentBase : IHasIntId, IHasName
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
﻿using Core.BaseModels.Interfaces;

namespace Core.BaseModels
{
    public class SubDivisionBase : IHasName, IHasIntId
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IsoCode { get; set; }
    }
}
﻿using Core.BaseModels.Interfaces;

namespace Core.BaseModels
{
    public class CountryBase : IHasIntId, IHasName
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string A2Code { get; set; }
    }
}
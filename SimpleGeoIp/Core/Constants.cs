﻿namespace Core
{
    public static class Constants
    {
        public static class SqlParameters
        {
            public static readonly string DefaultConnectionName = "GeoIpDbConnection";
            public static readonly int CommandTimeoutSeconds = 500;
        }
        
        public static class TableNames
        {
            public static readonly string GeoNamesTableName = "GeoNames";
            public static readonly string NetworksTableName = "Networks";
            public static readonly string ContinentsTableName = "Continents";
            public static readonly string CountriesTableName = "Countries";
            public static readonly string FirstSubDivisionsTableName = "FirstSubDivisions";
            public static readonly string SecondSubDivisionsTableName = "SecondSubDivisions";
            public static readonly string TimeZonesTableName = "TimeZones";
        }

    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Core.BaseModels;

namespace Core.Entities
{
    public class CountryEntity : CountryBase
    {
        public ICollection<NetworkEntity> NetworksRegistered { get; set; }
        public ICollection<NetworkEntity> NetworksRepresented { get; set; }
        public ICollection<GeoNameEntity> GeoNames { get; set; }

        public CountryEntity()
        {
            NetworksRegistered = new Collection<NetworkEntity>();
            NetworksRepresented = new Collection<NetworkEntity>();
            GeoNames = new Collection<GeoNameEntity>();
        }
    }
}
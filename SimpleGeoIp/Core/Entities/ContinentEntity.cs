﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Core.BaseModels;

namespace Core.Entities
{
    public class ContinentEntity : ContinentBase
    {
        public ICollection<GeoNameEntity> GeoNames { get; set; }

        public ContinentEntity()
        {
            GeoNames = new Collection<GeoNameEntity>();
        }
    }
}
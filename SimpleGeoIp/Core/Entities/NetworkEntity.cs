﻿using Core.BaseModels;

namespace Core.Entities
{
    public class NetworkEntity : NetworkBase
    {
        public GeoNameEntity GeoName { get; set; }
        public CountryEntity RegisteredCountry { get; set; }
        public CountryEntity RepresentedCountry { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Core.BaseModels;

namespace Core.Entities
{
    public class TimeZoneEntity : TimeZoneBase
    {
        public ICollection<GeoNameEntity> GeoNames { get; set; }

        public TimeZoneEntity()
        {
            GeoNames = new Collection<GeoNameEntity>();
        }
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Core.BaseModels;

namespace Core.Entities
{
    public class FirstSubDivisionEntity : SubDivisionBase
    {
        public ICollection<GeoNameEntity> GeoNames { get; set; }

        public FirstSubDivisionEntity()
        {
            GeoNames = new Collection<GeoNameEntity>();
        }
    }
}
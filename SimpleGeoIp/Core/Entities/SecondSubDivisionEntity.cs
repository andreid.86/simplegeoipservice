﻿using Core.BaseModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Core.Entities
{
    public class SecondSubDivisionEntity : SubDivisionBase
    {
        public ICollection<GeoNameEntity> GeoNames { get; set; }

        public SecondSubDivisionEntity()
        {
            GeoNames = new Collection<GeoNameEntity>();
        }
    }
}
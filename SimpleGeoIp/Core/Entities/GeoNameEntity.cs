﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Core.BaseModels;

namespace Core.Entities
{
    public class GeoNameEntity : GeoNameBase
    {
        public ContinentEntity Continent { get; set; }
        public CountryEntity Country { get; set; }
        public TimeZoneEntity TimeZone { get; set; }
        public FirstSubDivisionEntity FirstSubDivision { get; set; }
        public SecondSubDivisionEntity SecondSubDivision { get; set; }
        public ICollection<NetworkEntity> Networks { get; set; }

        public GeoNameEntity()
        {
            Networks = new Collection<NetworkEntity>();
        }
    }
}
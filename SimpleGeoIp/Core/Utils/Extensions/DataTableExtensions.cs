﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using NetTopologySuite.Geometries;

namespace Core.Utils.Extensions
{
    public static class DataTableExtensions
    {
        public static DataTable ToDataTable<TKey, TValue>(this IDictionary<TKey, TValue> data)
        {
            DataTable table = new DataTable();

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(TValue));
            
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.PropertyType == typeof(Point))
                {
                    table.Columns.Add(prop.Name);
                }
                else
                {
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
                
            }
            foreach (var (key, value) in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    try
                    {
                        if (prop.PropertyType == typeof(Point))
                        {
                            row[prop.Name] = prop.GetValue(value) ?? DBNull.Value;
                        }
                        else
                        {
                            row[prop.Name] = prop.GetValue(value) ?? DBNull.Value;
                        }
                    }
                    catch (Exception ex)
                    {
                        row[prop.Name] = DBNull.Value;
                    }
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
﻿using System;

namespace Core.Utils.Extensions
{
    public static class DateTimeExtensions
    {
        public static long ToUnixTimestamp(this DateTime date)
        {
            var result = ((DateTimeOffset)date).ToUnixTimeSeconds();
            return result;
        }

    }
}
﻿using System;
using System.Linq;
using System.Net;

namespace Core.Utils
{
    public static class IpHelpers
    {
        /// <summary>
        /// Method to get the start (min) IP address from string IPv4 CIDR Network Notation
        /// </summary>
        /// <param name="network">The IPv4 CIDR notation</param>
        /// <returns>Returns Int32 (including negative) value of the start (min) IP address based on provided IPv4 CIDR Network Notation</returns>
        public static int MinIp(string network)
        {
            string[] parts = network.Split('/');

            if (parts.Length != 2)
                throw new ArgumentException("Invalid network");

            if (!IsValidIp(parts[0]))
                throw new ArgumentException("Invalid IP address");

            int ip = IpToInt(parts[0]);

            int maskPrefix = Convert.ToInt32(parts[1]);

            if (!(0 <= maskPrefix && maskPrefix <= 32))
                throw new ArgumentException("Invalid network mask");


            // ~    - побитовое дополнение
            //      создает побитовое дополнение своего операнда путем инвертирования каждого бита

            // <<   - бинарный сдвиг
            // <<   - сдвигает левый операнд влево на количество битов, определенное правым операндом
            // &    - вычисляет побитовое логическое И своих операндов
            int mask = ~((1 << (32 - maskPrefix)) - 1);

            int x1 = 32 - maskPrefix;
            int x2 = 1 << x1;
            int x3 = x2 - 1;
            int x4 = ~x3;

            int net = ip & mask;
            int broadcast = net + ~mask;
            int minIp = net + 1;

            return minIp;
        }


        /// <summary>
        /// Method to get the end (max) IP address from string IPv4 CIDR Network Notation
        /// </summary>
        /// <param name="network">The IPv4 CIDR notation</param>
        /// <returns>Returns Int32 (including negative) value of the end (max) IP address based on provided IPv4 CIDR Network Notation</returns>
        public static int MaxIp(string network)
        {
            string[] parts = network.Split('/');

            if (parts.Length != 2)
                throw new ArgumentException("Invalid network");

            if (!IsValidIp(parts[0]))
                throw new ArgumentException("Invalid IP address");
            
            int ip = IpToInt(parts[0]);
            
            int bits = Convert.ToInt32(parts[1]);

            if (!(0 <= bits && bits <= 32))
                throw new ArgumentException("Invalid network mask");

            int mask = ~((1 << (32 - bits)) - 1);
            int net = ip & mask;
            int broadcast = net + ~mask;
            int maxIp = broadcast - 1;

            return maxIp;
        }


        public static bool IsValidIp(string ipAddress)
        {
            string[] parts = ipAddress.Split('.');

            return parts.Length == 4 && parts.All(part => (uint.TryParse((string) part, out uint intPart) && (intPart <= 255)));
        }

        public static bool IsPrivateIp(string ipAddress)
        {
            int[] ipParts = ipAddress.Split( '.' ).Where(x => int.TryParse(x, out _)).Select(int.Parse).ToArray();

            return ipParts[0] == 10 ||
                   (ipParts[0] == 192 && ipParts[1] == 168) ||
                   (ipParts[0] == 172 && (ipParts[1] >= 16 && ipParts[1] <= 31));
        }

        public static int IpToInt(string ip)
        {
            if (!IsValidIp(ip))
                throw new ArgumentException("Invalid IP Address");

            var x1 = IPAddress.Parse(ip).GetAddressBytes();
            var x2 = BitConverter.ToInt32(x1,0);
            var x3 = IPAddress.NetworkToHostOrder(x2);


            var result = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(IPAddress.Parse(ip).GetAddressBytes(), 0));
            return result;
        }

        public static string IntToIp(int intIp)
        {
            byte[] ip = BitConverter.GetBytes(intIp);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ip);
            }
            string address = string.Join(".", ip.Select(n => n.ToString()));
            return address;
        }


        private static uint ParseIp(string network)
        {
            string[] parts = network.Split('/');

            if (!IsValidIp(parts[0]))
                throw new ArgumentException("Invalid IP Address");

            string[] ipParts = parts[0].Split('.');
            
            uint[] ipOctets = new uint[4];

            for (int i = 0; i < 4; i++)
            {
                ipOctets[i] = Convert.ToUInt32(ipParts[i]) << (24 - 8 * i);
            }

            uint ip = ipOctets[0] | ipOctets[1] | ipOctets[2] | ipOctets[3];

            return ip;
        }

        private static uint ParseMask(string network)
        {
            string[] parts = network.Split('/');

            int maskBits = Convert.ToInt32(parts[1]);

            if (!(1 <= maskBits && maskBits <= 32))
            {
                throw new ArgumentException("Invalid IP Mask");
            }

            uint mask = 0xffffffff;
            mask <<= (32 - maskBits);

            return mask;
        }


    }
}
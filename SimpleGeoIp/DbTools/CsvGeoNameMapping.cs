﻿using System;
using TinyCsvParser;
using TinyCsvParser.Mapping;
using TinyCsvParser.Tokenizer.RFC4180;
using TinyCsvParser.TypeConverter;

namespace DbTools
{
    public sealed class CsvMappings
    {
        public static Options Options = new Options('"', '\\', ',');
        public static RFC4180Tokenizer Tokenizer = new RFC4180Tokenizer(Options);
        public static CsvParserOptions CsvParserOptions = new CsvParserOptions(true, Tokenizer);
        public static BoolConverter BoolConverter = new BoolConverter("1", "0", StringComparison.OrdinalIgnoreCase);


        public class CsvGeoName
        {
            public int GeoNameId { get; set; }
            public string LocaleCode { get; set; }
            public string ContinentCode { get; set; }
            public string ContinentName { get; set; }
            public string CountryIsoCode { get; set; }
            public string CountryName { get; set; }
            public string FirstSubdivisionIsoCode { get; set; }
            public string FirstSubdivisionName { get; set; }
            public string SecondSubdivisionIsoCode { get; set; }
            public string SecondSubdivisionName { get; set; }
            public string CityName { get; set; }
            public string MetroCode { get; set; }
            public string TimeZone { get; set; }
            public bool IsInEuropean { get; set; }

        }

        public class CsvNetwork
        {
            public string Network { get; set; }
            public int? GeoNameId { get; set; }
            public int? RegisteredCountryGeoNameId { get; set; }
            public int? RepresentedCountryGeoNameId { get; set; }
            public bool IsAnonymousProxy { get; set; }
            public bool IsSatelliteProvider { get; set; }
            public string PostalCode { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public int? AccuracyRadius { get; set; }
        }


        public class CsvGeoNameMapping : CsvMapping<CsvGeoName>
        {
            public CsvGeoNameMapping() : base()
            {
                MapProperty(0, g => g.GeoNameId);
                MapProperty(1, g => g.LocaleCode);
                MapProperty(2, g => g.ContinentCode);
                MapProperty(3, g => g.ContinentName);
                MapProperty(4, g => g.CountryIsoCode);
                MapProperty(5, g => g.CountryName);
                MapProperty(6, g => g.FirstSubdivisionIsoCode);
                MapProperty(7, g => g.FirstSubdivisionName);
                MapProperty(8, g => g.SecondSubdivisionIsoCode);
                MapProperty(9, g => g.SecondSubdivisionName);
                MapProperty(10, g => g.CityName);
                MapProperty(11, g => g.MetroCode);
                MapProperty(12, g => g.TimeZone);
                MapProperty(13, g => g.IsInEuropean, BoolConverter);
            }
        }

        public class CsvNetworkMapping : CsvMapping<CsvNetwork>
        {
            public CsvNetworkMapping() : base()
            {
                MapProperty(0, g => g.Network);
                MapProperty(1, g => g.GeoNameId);
                MapProperty(2, g => g.RegisteredCountryGeoNameId);
                MapProperty(3, g => g.RepresentedCountryGeoNameId);
                MapProperty(4, g => g.IsAnonymousProxy, BoolConverter);
                MapProperty(5, g => g.IsSatelliteProvider, BoolConverter);
                MapProperty(6, g => g.PostalCode);
                MapProperty(7, g => g.Latitude);
                MapProperty(8, g => g.Longitude);
                MapProperty(9, g => g.AccuracyRadius);
            }

        }
    }

}

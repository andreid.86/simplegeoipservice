﻿using Microsoft.Extensions.Configuration;

namespace DbTools
{
    public sealed class Configuration
    {
        private readonly IConfiguration _config;

        public Configuration(IConfiguration config)
        {
            _config = config;
        }

        public string ConnectionString => _config.GetConnectionString(Core.Constants.SqlParameters.DefaultConnectionName);
        public string GeoNamesDataSetFileName => _config.GetSection("InitialDataSets:GeoNames.FileName").Value;
        public string NetworksDataSetFileName => _config.GetSection("InitialDataSets:Networks.FileName").Value;
        public uint NetworkUpdatePackSize => _config.GetValue<uint>("NetworkUpdatePackSize", defaultValue:Constants.DefaultUpdatePackSize);
    }
}

﻿using Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using System;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace DbTools
{
    class Program
    {

        static void Main(string[] args)
        {

            var logger = LogManager.GetCurrentClassLogger();

            try
            {
                logger.Info("Application start");

                var config = new ConfigurationBuilder()
                    .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .Build();

                var serviceProvider = BuildContainer(config);

                using (serviceProvider as IDisposable)
                {
                    using (var dbContext = serviceProvider.GetRequiredService<DataContext>())
                    {
                        if (!dbContext.Database.GetService<IRelationalDatabaseCreator>().Exists())
                        {
                            logger.Info("Database does not exists. Migration started");
                            dbContext.Database.Migrate();
                        }
                        
                    }

                    var dbUpdater = serviceProvider.GetRequiredService<DbUpdater>();
                    dbUpdater.ProcessGeoNames();
                    dbUpdater.ProcessNetworks();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Application caused an exception");
                throw;
            }
            finally
            {
                LogManager.Shutdown();
            }

        }

        private static IServiceProvider BuildContainer(IConfiguration config)
        {
            var result = new ServiceCollection()
                    .AddSingleton<Configuration>()
                    .AddDbContext<DataContext>(options => options.UseSqlServer(config.GetConnectionString("GeoIpDbConnection"), o => o.UseNetTopologySuite()))
                    .AddSingleton<DbUpdater>()
                    .AddSingleton(config)
                    .AddLogging(loggingBuilder =>
                    {
                        loggingBuilder.ClearProviders();
                        loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Information);
                        loggingBuilder.AddNLog(config);

                    })
                    .BuildServiceProvider();
            return result;
        }
    }
}

﻿using Core.BaseModels;
using Core.Utils;
using Core.Utils.Extensions;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TinyCsvParser;

namespace DbTools
{
    public sealed class DbUpdater
    {
        private readonly Configuration _config;
        private readonly ILogger<DbUpdater> _logger;

        private ConcurrentDictionary<string, ContinentBase> _continents;
        private ConcurrentDictionary<string, CountryBase> _countries;
        private ConcurrentDictionary<string, SubDivisionBase> _firstSubDivisions;
        private ConcurrentDictionary<string, SubDivisionBase> _secondSubDivisions;
        private ConcurrentDictionary<string, TimeZoneBase> _timeZones;
        private ConcurrentDictionary<int, GeoNameBase> _geoNames;

        public DbUpdater(Configuration config, ILogger<DbUpdater> logger)
        {
            _config = config;
            _logger = logger;
        }

        public void ProcessGeoNames()
        {
            var geoNamesParser =
                new CsvParser<CsvMappings.CsvGeoName>(CsvMappings.CsvParserOptions,
                    new CsvMappings.CsvGeoNameMapping());
            var csvRecords = geoNamesParser.ReadFromFile(_config.GeoNamesDataSetFileName, Encoding.UTF8).ToList();
            var parsedGeoNames = csvRecords.Where(n => n.IsValid).Select(c => c.Result).ToList();

            UpdateContinents(parsedGeoNames);
            UpdateCountries(parsedGeoNames);
            UpdateFirstSubDivisions(parsedGeoNames);
            UpdateSecondSubDivisions(parsedGeoNames);
            UpdateTimeZones(parsedGeoNames);
            UpdateGeoNames(parsedGeoNames);

            _continents = null;
            _firstSubDivisions = null;
            _secondSubDivisions = null;
            _timeZones = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public void ProcessNetworks()
        {
            var geoNamesParser =
                new CsvParser<CsvMappings.CsvNetwork>(CsvMappings.CsvParserOptions,
                    new CsvMappings.CsvNetworkMapping());
            var csvRecords = geoNamesParser.ReadFromFile(_config.NetworksDataSetFileName, Encoding.UTF8).ToList();
            var parsedNetworks = csvRecords.Where(n => n.IsValid).Select(c => c.Result).ToList();

            UpdateNetworks(parsedNetworks);
        }

        private void UpdateContinents(List<CsvMappings.CsvGeoName> geoNames)
        {
            _logger.LogInformation("Start update continents");
            _continents = new ConcurrentDictionary<string, ContinentBase>(StringComparer.OrdinalIgnoreCase);

            using (var connection = new SqlConnection(_config.ConnectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        int newContinentsCount = 0;
                        var existentContinentsList = connection.Query<ContinentBase>(
                            $"SELECT * FROM {Core.Constants.TableNames.ContinentsTableName}",
                            transaction: transaction).ToList();


                        existentContinentsList.ForEach(continent =>
                        {
                            if (_continents.ContainsKey(continent.Code))
                            {
                                _logger.LogWarning(
                                    $"Duplicated continent found in the database, {continent.Id} - {continent.Code} ");
                            }
                            else
                            {
                                _continents[continent.Code] = new ContinentBase()
                                {
                                    Code = continent.Code,
                                    Name = continent.Name,
                                    Id = continent.Id
                                };
                            }
                        });

                        geoNames.ForEach(g =>
                        {
                            if (!_continents.ContainsKey(g.ContinentCode) && !string.IsNullOrEmpty(g.ContinentCode))
                            {
                                var continentId = connection.ExecuteScalar<int>(
                                    $@"INSERT INTO {Core.Constants.TableNames.ContinentsTableName} ([Name], [Code]) VALUES (@Name, @Code);
                                    SELECT CAST(SCOPE_IDENTITY() as int)",
                                    new { Name = g.ContinentName, Code = g.ContinentCode },
                                    transaction: transaction);

                                _continents[g.ContinentCode] = new ContinentBase()
                                {
                                    Code = g.ContinentCode,
                                    Name = g.ContinentName,
                                    Id = continentId
                                };
                                newContinentsCount++;
                            }

                        });

                        transaction.Commit();
                        _logger.LogInformation(
                            $"Continents successfully updated. {newContinentsCount} continents have been added");
                    }
                    catch
                    {
                        _logger.LogError("An error occurred while updating continents");
                        transaction.Rollback();
                        throw;
                    }
                }
            }

        }
        private void UpdateCountries(List<CsvMappings.CsvGeoName> geoNames)
        {
            _logger.LogInformation("Start update countries");
            _countries = new ConcurrentDictionary<string, CountryBase>(StringComparer.OrdinalIgnoreCase);

            using (var connection = new SqlConnection(_config.ConnectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        int newCountriesCount = 0;
                        var existentCountriesList = connection.Query<CountryBase>(
                            $"SELECT * FROM {Core.Constants.TableNames.CountriesTableName}",
                            transaction: transaction).ToList();


                        existentCountriesList.ForEach(country =>
                        {
                            if (_countries.ContainsKey(country.A2Code))
                            {
                                _logger.LogWarning(
                                    $"Duplicated country found in the database, {country.Id} - {country.A2Code} ");
                            }
                            else
                            {
                                _countries[country.A2Code] = new CountryBase()
                                {
                                    A2Code = country.A2Code,
                                    Name = country.Name,
                                    Id = country.Id
                                };
                            }
                        });

                        geoNames.ForEach(g =>
                        {

                            if (!_countries.ContainsKey(g.CountryIsoCode) && !string.IsNullOrEmpty(g.CountryIsoCode))
                            {
                                var countryId = connection.ExecuteScalar<int>(
                                    $@"INSERT INTO {Core.Constants.TableNames.CountriesTableName} ([Name], [A2Code]) VALUES (@Name, @A2Code);
                                    SELECT CAST(SCOPE_IDENTITY() as int)",
                                    new { Name = g.CountryName, A2Code = g.CountryIsoCode },
                                    transaction: transaction);

                                _countries[g.CountryIsoCode] = new CountryBase()
                                {
                                    A2Code = g.CountryIsoCode,
                                    Name = g.CountryName,
                                    Id = countryId
                                };
                                newCountriesCount++;
                            }

                        });

                        transaction.Commit();
                        _logger.LogInformation(
                            $"Countries successfully updated. {newCountriesCount} countries have been added");
                    }
                    catch
                    {
                        _logger.LogError("An error occurred while updating countries");
                        transaction.Rollback();
                        throw;
                    }
                }
            }

        }
        private void UpdateFirstSubDivisions(List<CsvMappings.CsvGeoName> geoNames)
        {
            _logger.LogInformation("Start update first subdivisions");
            _firstSubDivisions = new ConcurrentDictionary<string, SubDivisionBase>(StringComparer.OrdinalIgnoreCase);

            using (var connection = new SqlConnection(_config.ConnectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        int newFirstSubDivisions = 0;
                        var existentFirstSubDivisions = connection.Query<SubDivisionBase>(
                            $"SELECT * FROM {Core.Constants.TableNames.FirstSubDivisionsTableName}",
                            transaction: transaction).ToList();


                        existentFirstSubDivisions.ForEach(subDivision =>
                        {
                            if (_firstSubDivisions.ContainsKey(subDivision.IsoCode))
                            {
                                _logger.LogWarning(
                                    $"Duplicated first subdivision found in the database, {subDivision.Id} - {subDivision.IsoCode} ");
                            }
                            else
                            {
                                _firstSubDivisions[subDivision.IsoCode] = new SubDivisionBase()
                                {
                                    IsoCode = subDivision.IsoCode,
                                    Name = subDivision.Name,
                                    Id = subDivision.Id
                                };
                            }
                        });

                        geoNames.ForEach(g =>
                        {

                            if (!_firstSubDivisions.ContainsKey(g.FirstSubdivisionIsoCode) &&
                                !string.IsNullOrEmpty(g.FirstSubdivisionIsoCode))
                            {
                                var subDivisionId = connection.ExecuteScalar<int>(
                                    $@"INSERT INTO {Core.Constants.TableNames.FirstSubDivisionsTableName} ([Name], [IsoCode]) VALUES (@Name, @IsoCode);
                                    SELECT CAST(SCOPE_IDENTITY() as int)",
                                    new { Name = g.FirstSubdivisionName, IsoCode = g.FirstSubdivisionIsoCode },
                                    transaction: transaction);

                                _firstSubDivisions[g.FirstSubdivisionIsoCode] = new SubDivisionBase()
                                {
                                    IsoCode = g.FirstSubdivisionIsoCode,
                                    Name = g.FirstSubdivisionName,
                                    Id = subDivisionId
                                };
                                newFirstSubDivisions++;
                            }

                        });

                        transaction.Commit();
                        _logger.LogInformation(
                            $"First subdivisions successfully updated. {newFirstSubDivisions} subdivisions have been added");
                    }
                    catch
                    {
                        _logger.LogError("An error occurred while updating first subdivisions");
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
        private void UpdateSecondSubDivisions(List<CsvMappings.CsvGeoName> geoNames)
        {
            _logger.LogInformation("Start update second subdivisions");
            _secondSubDivisions = new ConcurrentDictionary<string, SubDivisionBase>(StringComparer.OrdinalIgnoreCase);

            using (var connection = new SqlConnection(_config.ConnectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        int newSecondSubDivisions = 0;
                        var existentSecondSubDivisions = connection.Query<SubDivisionBase>(
                            $"SELECT * FROM {Core.Constants.TableNames.SecondSubDivisionsTableName}",
                            transaction: transaction).ToList();


                        existentSecondSubDivisions.ForEach(subDivision =>
                        {
                            if (_secondSubDivisions.ContainsKey(subDivision.IsoCode))
                            {
                                _logger.LogWarning(
                                    $"Duplicated second subdivision found in the database, {subDivision.Id} - {subDivision.IsoCode} ");
                            }
                            else
                            {
                                _secondSubDivisions[subDivision.IsoCode] = new SubDivisionBase()
                                {
                                    IsoCode = subDivision.IsoCode,
                                    Name = subDivision.Name,
                                    Id = subDivision.Id
                                };
                            }
                        });

                        geoNames.ForEach(g =>
                        {

                            if (!_secondSubDivisions.ContainsKey(g.SecondSubdivisionIsoCode) &&
                                !string.IsNullOrEmpty(g.SecondSubdivisionIsoCode))
                            {
                                var subDivisionId = connection.ExecuteScalar<int>(
                                    $@"INSERT INTO {Core.Constants.TableNames.SecondSubDivisionsTableName} ([Name], [IsoCode]) VALUES (@Name, @IsoCode);
                                    SELECT CAST(SCOPE_IDENTITY() as int)",
                                    new { Name = g.SecondSubdivisionName, IsoCode = g.SecondSubdivisionIsoCode },
                                    transaction: transaction);

                                _secondSubDivisions[g.SecondSubdivisionIsoCode] = new SubDivisionBase()
                                {
                                    IsoCode = g.SecondSubdivisionIsoCode,
                                    Name = g.SecondSubdivisionName,
                                    Id = subDivisionId
                                };
                                newSecondSubDivisions++;
                            }

                        });

                        transaction.Commit();
                        _logger.LogInformation(
                            $"Second subdivisions successfully updated. {newSecondSubDivisions} subdivisions have been added");
                    }
                    catch
                    {
                        _logger.LogError("An error occurred while updating second subdivisions");
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
        private void UpdateTimeZones(List<CsvMappings.CsvGeoName> geoNames)
        {
            _logger.LogInformation("Start update time zones");
            _timeZones = new ConcurrentDictionary<string, TimeZoneBase>(StringComparer.OrdinalIgnoreCase);

            using (var connection = new SqlConnection(_config.ConnectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        int newTimeZones = 0;
                        var existentTimeZones = connection.Query<TimeZoneBase>(
                            $"SELECT * FROM {Core.Constants.TableNames.TimeZonesTableName}",
                            transaction: transaction).ToList();


                        existentTimeZones.ForEach(timezone =>
                        {
                            if (_timeZones.ContainsKey(timezone.Name))
                            {
                                _logger.LogWarning(
                                    $"Duplicated time zone found in the database, {timezone.Id} - {timezone.Name} ");
                            }
                            else
                            {
                                _timeZones[timezone.Name] = new TimeZoneBase()
                                {
                                    Name = timezone.Name,
                                    Id = timezone.Id
                                };
                            }
                        });

                        geoNames.ForEach(g =>
                        {

                            if (!_timeZones.ContainsKey(g.TimeZone) && !string.IsNullOrEmpty(g.TimeZone))
                            {
                                var timeZoneId = connection.ExecuteScalar<int>(
                                    $@"INSERT INTO {Core.Constants.TableNames.TimeZonesTableName} ([Name]) VALUES (@Name);
                                    SELECT CAST(SCOPE_IDENTITY() as int)",
                                    new { Name = g.TimeZone },
                                    transaction: transaction);

                                _timeZones[g.TimeZone] = new TimeZoneBase()
                                {
                                    Name = g.TimeZone,
                                    Id = timeZoneId
                                };
                                newTimeZones++;
                            }

                        });

                        transaction.Commit();
                        _logger.LogInformation(
                            $"Time zones successfully updated. {newTimeZones} time zones have been added");
                    }
                    catch
                    {
                        _logger.LogError("An error occurred while updating time zones");
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
        private void UpdateGeoNames(List<CsvMappings.CsvGeoName> geoNames)
        {
            _logger.LogInformation("Start update geo names");
            _geoNames = new ConcurrentDictionary<int, GeoNameBase>();

            geoNames.ForEach(geoName =>
            {
                if (!_geoNames.ContainsKey(geoName.GeoNameId) &&
                    !string.IsNullOrWhiteSpace(geoName.ContinentCode) &&
                    !string.IsNullOrWhiteSpace(geoName.CountryIsoCode) &&
                    !string.IsNullOrWhiteSpace(geoName.TimeZone))
                {
                    _geoNames[geoName.GeoNameId] = new GeoNameBase()
                    {
                        Id = geoName.GeoNameId,
                        ContinentId = _continents[geoName.ContinentCode].Id,
                        CountryId = _countries[geoName.CountryIsoCode].Id,
                        CityName = !string.IsNullOrWhiteSpace(geoName.CityName) ? geoName.CityName : null,
                        FirstSubDivisionId = !string.IsNullOrWhiteSpace(geoName.FirstSubdivisionIsoCode)
                            ? _firstSubDivisions[geoName.FirstSubdivisionIsoCode].Id
                            : (int?)null,
                        SecondSubDivisionId = !string.IsNullOrWhiteSpace(geoName.SecondSubdivisionIsoCode)
                            ? _secondSubDivisions[geoName.SecondSubdivisionIsoCode].Id
                            : (int?)null,
                        IsInEuropeanUnion = geoName.IsInEuropean,
                        TimeZoneId = _timeZones[geoName.TimeZone].Id
                    };
                }
                else
                {
                    _logger.LogWarning($"Incomplete geo name {geoName.GeoNameId }");
                }

            });

            using (var connection = new SqlConnection(_config.ConnectionString))
            {
                DynamicParameters requestParameters = new DynamicParameters();
                requestParameters.Add("@geoNames", _geoNames.ToDataTable().AsTableValuedParameter("[dbo].[GeoNameTableType]"));

                try
                {
                    var result = connection.Execute(
                        $"[dbo].[UpdateGeoNames]",
                        commandType: CommandType.StoredProcedure,
                        param: requestParameters,
                        commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds);
                    _logger.LogInformation($"Geo names successfully updated");
                }
                catch
                {
                    _logger.LogError("An error occurred while updating geo names");
                    throw;
                }
            }
        }
        private void UpdateNetworks(IReadOnlyCollection<CsvMappings.CsvNetwork> networks)
        {
            var sw = new Stopwatch();
            sw.Start();

            _logger.LogInformation("Start update networks");
            
            var packNum = 0;
            var networksCount = networks.Count;
            
            _logger.LogInformation($@"Networks count is - {networksCount}");

            var packSize = (int) _config.NetworkUpdatePackSize;

            _logger.LogInformation($@"Pack size - { packSize}");
            
            while (packNum * packSize < networksCount)
            {
                packNum++;
                var skip = packSize * (packNum - 1);

                var networksDict = new ConcurrentDictionary<string, NetworkBase>(StringComparer.OrdinalIgnoreCase);

                networks.Skip(skip).Take(packSize).ToList().ForEach(network =>
                {

                    if (networksDict.ContainsKey(network.Network)) return;

                    int minIpAddress, maxIpAddress;

                    try
                    {
                        minIpAddress = IpHelpers.MinIp(network.Network);
                        maxIpAddress = IpHelpers.MaxIp(network.Network);
                        if (maxIpAddress == minIpAddress) return;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogWarning(
                            $"Could not convert network ip ranges for network {network.Network}, {ex.Message}");
                        return;
                    }

                    Point location = null;
                    try
                    {
                        if (network.Latitude.HasValue && network.Longitude.HasValue)
                        {
                            location = new Point(network.Longitude.Value, network.Latitude.Value);
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.LogWarning($"Could not parse location for network {network.Network}, {ex.Message}");
                    }

                    var geoNameId = (network.GeoNameId.HasValue &&
                                     _geoNames.ContainsKey(network.GeoNameId.Value))
                        ? _geoNames[network.GeoNameId.Value].Id
                        : (long?)null;

                    var registeredCountryId = (network.RegisteredCountryGeoNameId.HasValue &&
                                               _geoNames.ContainsKey(network.RegisteredCountryGeoNameId.Value))
                        ? _geoNames[network.RegisteredCountryGeoNameId.Value].CountryId
                        : (int?)null;

                    var representedCountryId = (network.RepresentedCountryGeoNameId.HasValue &&
                                                _geoNames.ContainsKey(network.RepresentedCountryGeoNameId.Value))
                        ? _geoNames[network.RepresentedCountryGeoNameId.Value].CountryId
                        : (int?)null;

                    networksDict[network.Network] = new NetworkBase()
                    {
                        Name = network.Network,
                        GeoNameId = geoNameId,
                        RegisteredCountryId = registeredCountryId,
                        RepresentedCountryId = representedCountryId,
                        AccuracyRadius = network.AccuracyRadius,
                        MinIpAddress = minIpAddress,
                        MaxIpAddress = maxIpAddress,
                        Location = location
                    };

                });

                using (var connection = new SqlConnection(_config.ConnectionString))
                {

                    DynamicParameters requestParameters = new DynamicParameters();
                    requestParameters.Add("@networks", networksDict.ToDataTable().AsTableValuedParameter("[dbo].[NetworkTableType]"));

                    try
                    {
                        var result = connection.Execute(
                            $"[dbo].[UpdateNetworks]",
                            commandType: CommandType.StoredProcedure,
                            param: requestParameters,
                            commandTimeout: Core.Constants.SqlParameters.CommandTimeoutSeconds);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("An error occurred while updating networks");
                        throw;
                    }

                }
                _logger.LogInformation($"Pack {packNum} has been processed");

            }

            sw.Stop();
            _logger.LogInformation($"All networks have been processed. Elapsed time {sw.Elapsed.TotalMinutes} minutes");
        }
    }
}